<!-- Put a brief summary of the issue in the title. -->

# My system

- Distribution (name and version):
- GNOME Shell version:
- I installed the extension: <!-- Manually? From extensions.gnome.org? From my distribution? -->

# My problem

## Description of the problem


## Steps to reproduce

1.


<!-- Don't remove the following line -->
/label ~"bug::not confirmed"
